package io.convertmoney.jobs.bcb;

import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import io.convertmoney.domain.model.BbcCotacao;
import io.convertmoney.domain.repository.CacheBbcCotacoesRepository;
import io.convertmoney.domain.service.ApiBancoBrasilService;
import io.convertmoney.domain.utils.DateUtils;

@Component
public class BcbJobService {

	@Autowired
	private ApiBancoBrasilService service;

	@Autowired
	private CacheBbcCotacoesRepository repository;

	@Scheduled(cron = "0 0/1 * * * *")
	public void executar() {

//		String dataCotacao = DateUtils.getDataAtual(DateUtils.MM_DD_YYYY);
		String dataCotacao = "04-09-2020";

		service.getMoedas().getValue().forEach(moeda -> {

			try {

				BbcCotacao cotacao = service.getCotacao(moeda.getSimbolo(), dataCotacao);

				if (cotacao != null) {
					repository.save(cotacao);
				}

				BbcCotacao cotacoesPersistidas = repository.find(moeda.getSimbolo() + "-" + dataCotacao);

				System.out.println(cotacoesPersistidas.toString());

			} catch (URISyntaxException e) {
				e.printStackTrace();
			}

		});

	}

}
