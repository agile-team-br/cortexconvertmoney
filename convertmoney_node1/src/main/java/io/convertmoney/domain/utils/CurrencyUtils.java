package io.convertmoney.domain.utils;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class CurrencyUtils {
	
	
	public static Double convert(Double cotacaoOrigem, Double cotacaoDestino, Double valor) {
		
		Double taxa = cotacaoOrigem / cotacaoDestino;
		return arredondar(valor * taxa);
		
	}
	
	private static Double arredondar(double media) {
		DecimalFormat df = new DecimalFormat("0.00");
		df.setRoundingMode(RoundingMode.HALF_UP);
		return Double.valueOf(df.format(media));
	}

}
