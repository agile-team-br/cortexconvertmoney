package io.convertmoney.domain.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {
	
	
	static Locale ptBr = new Locale("pt", "BR");
	
	public static final SimpleDateFormat MM_DD_YYYY = new SimpleDateFormat("MM-dd-yyyy", ptBr);
	
	
	public static String getDataAtual(SimpleDateFormat formato) {
		return formato.format(Calendar.getInstance().getTime());
	}
	
	
	public static String format(Date data, SimpleDateFormat formato) {
		if (data != null) {
			return formato.format(data);
		}
		return "";
	}

}
