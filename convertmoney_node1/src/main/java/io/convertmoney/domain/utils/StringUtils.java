package io.convertmoney.domain.utils;

import java.util.UUID;

public class StringUtils {
	
	public static String randomUUID() {
		return UUID.randomUUID().toString().replace("-", "").substring(12);
	}


}
