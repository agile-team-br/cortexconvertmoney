package io.convertmoney.domain.service.impl;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import io.convertmoney.domain.model.BbcCotacao;
import io.convertmoney.domain.model.dto.CotacoesDto;
import io.convertmoney.domain.model.dto.MoedasDto;
import io.convertmoney.domain.service.ApiBancoBrasilService;

@Service
public class ApiBancoBrasilServiceImpl implements ApiBancoBrasilService {

	private RestTemplate restTemplate;

	public ApiBancoBrasilServiceImpl(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	@Override
	public MoedasDto getMoedas() {

		String url = "https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/Moedas";

		return restTemplate.getForObject(url, MoedasDto.class);
	}

	public BbcCotacao getCotacao(String moeda, String dataCotacao) throws URISyntaxException {

		CotacoesDto cotacoes = new CotacoesDto();

		String idBbcCotacao = moeda + "-" + dataCotacao;
		Double valorCotacao;

		String path = "https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/CotacaoMoedaDia(moeda=@moeda,dataCotacao=@dataCotacao)?%40moeda=";
		URI u = new URI(path + "'" + moeda + "'" + "&%40dataCotacao=" + "'" + dataCotacao + "'");

		cotacoes = restTemplate.getForObject(u, CotacoesDto.class);

		if (!cotacoes.getValue().isEmpty()) {

			if (cotacoes.getValue().size() < 4) {
				valorCotacao = cotacoes.getValue().get(0).getCotacaoVenda();
			} else {
				valorCotacao = cotacoes.getValue().get(4).getCotacaoVenda();
			}

			BbcCotacao cotacao = new BbcCotacao(idBbcCotacao, dataCotacao, moeda, valorCotacao);

			return cotacao;
		} else {
			return null;
		}

	}

}
