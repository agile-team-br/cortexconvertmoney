package io.convertmoney.domain.model;

import java.io.Serializable;
import java.util.Date;

import io.convertmoney.domain.utils.DateUtils;

public class BbcCotacao implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	
	private String dataCotacao;
	
	private String moeda;

	private Double valor;
	
	public BbcCotacao(String id, String dataCotacao, String moeda, Double valor) {
		this.id = id;
		this.dataCotacao = dataCotacao;
		this.moeda = moeda;
		this.valor = valor;
	}
	
	public BbcCotacao() {}
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDataCotacao() {
		return dataCotacao;
	}

	public void setDataCotacao(String dataCotacao) {
		this.dataCotacao = dataCotacao;
	}

	public String getMoeda() {
		return moeda;
	}

	public void setMoeda(String moeda) {
		this.moeda = moeda;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	public void geraId(String moeda, Date data) {
		this.id = moeda + "-" + DateUtils.format(data, DateUtils.MM_DD_YYYY);
	}

	@Override
	public String toString() {
		return "BbcCotacao [id=" + id + ", dataCotacao=" + dataCotacao + ", moeda=" + moeda + ", valor=" + valor + "]";
	}
	
	
	

	

}
