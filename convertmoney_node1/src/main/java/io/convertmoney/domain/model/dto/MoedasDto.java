package io.convertmoney.domain.model.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MoedasDto {
	
	private List<MoedaDto> value = new ArrayList<MoedaDto>();

	public List<MoedaDto> getValue() {
		return value;
	}

	public void setValue(List<MoedaDto> value) {
		this.value = value;
	}

}


