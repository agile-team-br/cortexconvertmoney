package io.convertmoney.domain.model.dto;

public class CotacaoDto {
	
	private Integer paridadeCompra;
	private Integer paridadeVenda;
    private Double cotacaoCompra;
    private Double cotacaoVenda;
    private String dataHoraCotacao;
    private String tipoBoletim;
    
    
	public Integer getParidadeCompra() {
		return paridadeCompra;
	}
	public void setParidadeCompra(Integer paridadeCompra) {
		this.paridadeCompra = paridadeCompra;
	}
	public Integer getParidadeVenda() {
		return paridadeVenda;
	}
	public void setParidadeVenda(Integer paridadeVenda) {
		this.paridadeVenda = paridadeVenda;
	}
	public String getDataHoraCotacao() {
		return dataHoraCotacao;
	}
	public void setDataHoraCotacao(String dataHoraCotacao) {
		this.dataHoraCotacao = dataHoraCotacao;
	}
	public String getTipoBoletim() {
		return tipoBoletim;
	}
	public void setTipoBoletim(String tipoBoletim) {
		this.tipoBoletim = tipoBoletim;
	}
	public Double getCotacaoCompra() {
		return cotacaoCompra;
	}
	public void setCotacaoCompra(Double cotacaoCompra) {
		this.cotacaoCompra = cotacaoCompra;
	}
	public Double getCotacaoVenda() {
		return cotacaoVenda;
	}
	public void setCotacaoVenda(Double cotacaoVenda) {
		this.cotacaoVenda = cotacaoVenda;
	}
    
	

}
