package io.convertmoney.domain.model.dto;

import java.io.Serializable;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class ConvertDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	
	@NotEmpty
	@Length(min=10, max=10, message = "A data deve ser no formato dd-MM-AAAA")
	private String dataCotacao;

	@NotEmpty
	@Length(min=3, max=3, message = "Você deve colocar uma moeda de origem")
	private String moedaOrigem;

	@NotEmpty
	@Length(min=3, max=3, message = "Você deve colocar uma moeda final que vai ser convertida")
	private String moedaFinal;

	private Double valor;
	
	private Boolean cacheable = false;

	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDataCotacao() {
		return dataCotacao;
	}

	public void setDataCotacao(String dataCotacao) {
		this.dataCotacao = dataCotacao;
	}

	public String getMoedaOrigem() {
		return moedaOrigem;
	}

	public void setMoedaOrigem(String moedaOrigem) {
		this.moedaOrigem = moedaOrigem;
	}

	public String getMoedaFinal() {
		return moedaFinal;
	}

	public void setMoedaFinal(String moedaFinal) {
		this.moedaFinal = moedaFinal;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public boolean moedaOrigemEreal() {
		return this.getMoedaOrigem().equals("BRL");
	}
	
	public boolean moedaFinalEreal() {
		return this.getMoedaFinal().equals("BRL");
	}

	public Boolean getCacheable() {
		return cacheable;
	}

	public void setCacheable(Boolean cacheable) {
		this.cacheable = cacheable;
	}
	

}
