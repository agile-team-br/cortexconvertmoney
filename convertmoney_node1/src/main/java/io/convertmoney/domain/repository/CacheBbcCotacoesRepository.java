package io.convertmoney.domain.repository;

import io.convertmoney.domain.model.BbcCotacao;

public interface CacheBbcCotacoesRepository {
	
	void save(BbcCotacao cotacao);
	BbcCotacao find(String id);

}
