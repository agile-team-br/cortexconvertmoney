package io.convertmoney.integrationTests.domain.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import io.convertmoney.domain.utils.CurrencyUtils;

public class ConvertTest {

	Double ctDolar = 5.1109;
	Double ctEuro = 5.6532;
	Double ctYene = 0.04726;

	@Test
	public void convertDolarToReal() {

		Double valorAConverter = 200.0;
        assertEquals(new Double(1022.18), CurrencyUtils.convert(ctDolar, 1.0, valorAConverter));
	}

	@Test
	public void convertRealToDolar() {
		Double valorAConverter = 1.0;
		assertEquals(new Double(0.2), CurrencyUtils.convert(1.0, ctDolar, valorAConverter));
	}

	@Test
	public void convertDolarToEuro() {
		
		Double valorAConverter = 200.0;
		assertEquals(new Double(180.81), CurrencyUtils.convert(ctDolar, ctEuro, valorAConverter));

	}

	@Test
	public void convertRealToYene() {

		Double valorAConverter = 200.0;
		assertEquals(new Double(4231.91), CurrencyUtils.convert(1.0, ctYene, valorAConverter));

	}



}
