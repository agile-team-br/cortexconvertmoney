# API CONVERT MONEY (Node 1)

Esse projeto é reponsável por escutar a fila, tratar as converções e consultas por schedule nas APIs externas do Banco do Brasil e armazenando as cotas e resultados no cache do Redis.

## Instalação

_Prerequisitos:_
* docker version 1.10.3
* docker-compose version 1.6.2


## Stack of frameworks/technologies:

* Spring Boot
* Docker
* Maven
* Redis
* RabbitMQ


## Débitos Técnicos:

* Testes




   






