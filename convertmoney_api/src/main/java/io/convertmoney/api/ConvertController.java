package io.convertmoney.api;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.convertmoney.domain.model.ResponseApi;
import io.convertmoney.domain.model.dto.ConvertDto;
import io.convertmoney.domain.repository.CacheConversoesRepository;
import io.convertmoney.domain.service.RabbitMQSender;
import io.convertmoney.domain.utils.StringUtils;

@RestController
public class ConvertController {
	
	private static final Logger log = LoggerFactory.getLogger(ConvertController.class);
	
	@Autowired
	RabbitMQSender rabbitMQSender;
	
	@Autowired
	CacheConversoesRepository repository;

	@CrossOrigin(origins = "*")
	@PostMapping
	public ResponseEntity<ResponseApi<CallBackUrl>> convert(@Valid @RequestBody ConvertDto dto, BindingResult result) {
	
		ResponseApi<CallBackUrl> response = new ResponseApi<CallBackUrl>();
		
		if(result.hasErrors()) {
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()) );
			return ResponseEntity.badRequest().body(response);
		}

		try {
			
			String id = StringUtils.randomUUID();
			dto.setId(id);
			rabbitMQSender.send(dto);
			
			CallBackUrl webhook = new CallBackUrl();
			webhook.setId(id);
			webhook.setUrl("http://localhost:8080/api-convertmoney/" + id);
			
			response.setData(webhook);
			return ResponseEntity.ok(response);
			
		} catch (Exception ex) {
			log.error("Ocorreu um erro ao enviar para a fila", ex);
			return ResponseEntity.badRequest().body(response);
		}
		
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(value = "/{id}")
	public ResponseEntity<ResponseApi<ConvertDto>> getConverter(@PathVariable("id") String id) {
	
		ResponseApi<ConvertDto> response = new ResponseApi<ConvertDto>();
		
		try {
			
			ConvertDto result = repository.find(id);
			response.setData(result);
			return ResponseEntity.ok(response);
			
		} catch (Exception ex) {
			log.error("Erro ao recuperar Convert", ex);
			return ResponseEntity.badRequest().body(response);
		}
		
	}
	

}

class CallBackUrl {
	
	private String url;
	
	private String id;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
}
