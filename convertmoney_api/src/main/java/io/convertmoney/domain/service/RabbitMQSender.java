package io.convertmoney.domain.service;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import io.convertmoney.domain.model.dto.ConvertDto;

@Service
public class RabbitMQSender {
	
	@Autowired
	private AmqpTemplate amqpTemplate;
	
	@Value("${convert.rabbitmq.exchange}")
	private String exchange;
	
	@Value("${convert.rabbitmq.routingkey}")
	private String routingkey;	
	
	public void send(ConvertDto company) {
		amqpTemplate.convertAndSend(exchange, routingkey, company);
		System.out.println("Send msg = " + company);
	    
	}
}