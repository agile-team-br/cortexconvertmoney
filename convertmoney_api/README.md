# API CONVERT MONEY (Gateway Rest API)

Esse projeto é o gateway com os serviços de integração entre sistemas e usuários, ele apenas se comunica com o RabbitMQ e com o cache no Redis.

## Instalação

_Prerequisitos:_
* docker version 1.10.3
* docker-compose version 1.6.2


## Stack of frameworks/technologies:

* Spring Boot
* Docker
* Maven
* Redis
* RabbitMQ


## Débitos Técnicos:

* Testes




   






