package io.convertmoney.domain.service;

import java.net.URISyntaxException;

import io.convertmoney.domain.model.BbcCotacao;
import io.convertmoney.domain.model.dto.MoedasDto;

public interface ApiBancoBrasilService {

	
	public MoedasDto getMoedas();
	
	public BbcCotacao getCotacao(String moeda, String dataCotacao) throws URISyntaxException;
	
}
