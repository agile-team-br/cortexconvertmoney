package io.convertmoney.domain.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.convertmoney.domain.model.BbcCotacao;
import io.convertmoney.domain.model.dto.ConvertDto;
import io.convertmoney.domain.repository.CacheBbcCotacoesRepository;
import io.convertmoney.domain.repository.CacheConversoesRepository;
import io.convertmoney.domain.service.ApiBancoBrasilService;
import io.convertmoney.domain.service.ConvertService;
import io.convertmoney.domain.utils.CurrencyUtils;

@Service
public class ConvertServiceImpl implements ConvertService{
	
	private static final Logger log = LoggerFactory.getLogger(ConvertServiceImpl.class);
	
	
	@Autowired
	private CacheBbcCotacoesRepository cacheBbcCotacoes;
	
	@Autowired
	private CacheConversoesRepository cacheConversoes;
	
	@Autowired 
	private ApiBancoBrasilService apiBancoBrasilService;

	@Override
	public ConvertDto convert(ConvertDto dto) {
	
		try {
			
			BbcCotacao cotacaoOrigem = null;
			BbcCotacao cotacaoFinal = null;
			BbcCotacao cotacao = null;
			String dataCotacao = dto.getDataCotacao();
			
			if(dto.moedaOrigemEreal()) { 
				cotacao =  cacheBbcCotacoes.find(dto.getMoedaFinal() + "-" + dataCotacao);
				
				if(cotacao == null) {
					cotacao = apiBancoBrasilService.getCotacao(dto.getMoedaFinal(), dataCotacao);
					cacheBbcCotacoes.save(cotacao);
					dto.setCacheable(true);
				}
				
				dto.setValor(CurrencyUtils.convert(1.0, cotacao.getValor(), dto.getValor()));
				
			} else if(dto.moedaFinalEreal()) {
				
				cotacao =  cacheBbcCotacoes.find(dto.getMoedaOrigem() + "-" + dataCotacao);
				
				if(cotacao == null) {
					cotacao = apiBancoBrasilService.getCotacao(dto.getMoedaOrigem(), dataCotacao);
					cacheBbcCotacoes.save(cotacao);
					dto.setCacheable(true);
				}
				dto.setValor(CurrencyUtils.convert(cotacao.getValor(), 1.0, dto.getValor()));
				
			} else {
				
				cotacaoOrigem = cacheBbcCotacoes.find(dto.getMoedaOrigem() + "-" + dataCotacao);
				if(cotacaoOrigem == null) {
					cotacaoOrigem = apiBancoBrasilService.getCotacao(dto.getMoedaOrigem(), dataCotacao);
					cacheBbcCotacoes.save(cotacaoOrigem);
				}
				cotacaoFinal = cacheBbcCotacoes.find(dto.getMoedaFinal() + "-" + dataCotacao);
				if(cotacaoFinal == null) {
					cotacaoFinal = apiBancoBrasilService.getCotacao(dto.getMoedaFinal(), dataCotacao);
					cacheBbcCotacoes.save(cotacaoFinal);
				}
				dto.setCacheable(true);
				
				dto.setValor(CurrencyUtils.convert(cotacaoOrigem.getValor(), cotacaoFinal.getValor(), dto.getValor()));
			}
				
			
		} catch (Exception e) {
			log.error("Ocorreu um erro na conversão de moeda", e);
		}
		
		
		return dto;
	}

	@Override
	public void saveInCache(ConvertDto dto) {
		cacheConversoes.save(dto);
		
	}

	@Override
	public ConvertDto getByIdInCache(String id) {
		return cacheConversoes.find(id);
	}


}
