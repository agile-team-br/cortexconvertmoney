package io.convertmoney.domain.service;

import io.convertmoney.domain.model.dto.ConvertDto;

public interface ConvertService {
	
	public ConvertDto convert(ConvertDto dto);
	
	public void saveInCache(ConvertDto dto);
	
	public ConvertDto getByIdInCache(String id);
	
}
