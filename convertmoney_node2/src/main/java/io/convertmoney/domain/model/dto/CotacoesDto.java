package io.convertmoney.domain.model.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CotacoesDto {
	
	List<CotacaoDto> value = new ArrayList<CotacaoDto>();

	public List<CotacaoDto> getValue() {
		return value;
	}

	public void setValue(List<CotacaoDto> value) {
		this.value = value;
	}

}


