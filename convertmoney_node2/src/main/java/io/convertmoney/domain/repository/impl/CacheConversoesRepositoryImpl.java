package io.convertmoney.domain.repository.impl;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import io.convertmoney.domain.model.dto.ConvertDto;
import io.convertmoney.domain.repository.CacheConversoesRepository;

@Repository
public class CacheConversoesRepositoryImpl implements CacheConversoesRepository{
	
	
	private static final String KEY = "CONVERSOES";

	private RedisTemplate<String, Object> redisTemplate;
	private HashOperations<String, String, ConvertDto> hashOperations;
	
	@Autowired
	public CacheConversoesRepositoryImpl(RedisTemplate<String, Object> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	@PostConstruct
	private void init() {
		hashOperations = redisTemplate.opsForHash();
	}

	@Override
	public void save(ConvertDto dto) {
		hashOperations.put(KEY, dto.getId(), dto);
		
	}

	@Override
	public ConvertDto find(String id) {
		return hashOperations.get(KEY, id);
	}
	
	

}
