package io.convertmoney.domain.repository.impl;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import io.convertmoney.domain.model.BbcCotacao;
import io.convertmoney.domain.repository.CacheBbcCotacoesRepository;


@Repository
public class CacheBbcCotacoesRepositoryImpl implements CacheBbcCotacoesRepository{
	
	
	private static final String KEY = "COTACOES";

	private RedisTemplate<String, Object> redisTemplate;
	private HashOperations<String, String, BbcCotacao> hashOperations;
	
	
	@Autowired
	public CacheBbcCotacoesRepositoryImpl(RedisTemplate<String, Object> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	@PostConstruct
	private void init() {
		hashOperations = redisTemplate.opsForHash();
	}

	@Override
	public void save(BbcCotacao cotacao) {
		hashOperations.put(KEY, cotacao.getId(), cotacao);
		
	}
	
	@Override
	public BbcCotacao find(String id) {
		return hashOperations.get(KEY, id);
	}

}
