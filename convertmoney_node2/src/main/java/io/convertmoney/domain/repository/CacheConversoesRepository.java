package io.convertmoney.domain.repository;

import io.convertmoney.domain.model.dto.ConvertDto;

public interface CacheConversoesRepository {
	
	void save(ConvertDto dto);
	ConvertDto find(String id);

}
